# Low_Mass_Zprime_Fitting

Use TRExFitter to get the fitting result.

.config is the configure file fed to TRExFitter framwork.

runfit.cxx can automatically run fitting for different mass point. num_threads set maxmuim number of threads can be used.

To run fitting, first compile runfit.cxx: g++ -std=c++11 runfit.cxx -lpthread -o runfit, then ./runfit