#/bin/bash
var=$1
mass=$2
weight=$3
downlm=$4
uplm=$5
mymasscut=$6
mynnvar=$7
mynncut=$8
crup=$9
cp config/Zprime_C.config.bak config/Zprime_${mass}GeV.config

sed -i "s/MASS/$mass/g" config/Zprime_${mass}GeV.config
sed -i "s/MYVAR/$var/g" config/Zprime_${mass}GeV.config
sed -i "s/UPLM/$uplm/g" config/Zprime_${mass}GeV.config
sed -i "s/DOWNLM/$downlm/g" config/Zprime_${mass}GeV.config
sed -i "s/MYWEIGHT/$weight/g" config/Zprime_${mass}GeV.config
sed -i "s/MyMassCut/$mymasscut/g" config/Zprime_${mass}GeV.config
sed -i "s/MYNNVAR/$mynnvar/g" config/Zprime_${mass}GeV.config
sed -i "s/MYNNCUT/$mynncut/g" config/Zprime_${mass}GeV.config
sed -i "s/CRUP/$crup/g" config/Zprime_${mass}GeV.config
trex-fitter n config/Zprime_${mass}GeV.config>logs/log${mass}GeV.txt
trex-fitter d config/Zprime_${mass}GeV.config>logs/log${mass}GeV.txt
trex-fitter w config/Zprime_${mass}GeV.config>logs/log${mass}GeV.txt
trex-fitter f config/Zprime_${mass}GeV.config>logs/log${mass}GeV.txt
trex-fitter p config/Zprime_${mass}GeV.config>logs/log${mass}GeV.txt
trex-fitter l config/Zprime_${mass}GeV.config>logs/log${mass}GeV.txt
trex-fitter s config/Zprime_${mass}GeV.config>logs/log${mass}GeV.txt
trex-fitter x config/Zprime_${mass}GeV.config>logs/log${mass}GeV.txt
#trex-fitter r config/Zprime_${mass}GeV.config>logs/log${mass}GeV.txt



