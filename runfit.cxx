#include<iostream>
#include<fstream>
#include<stdlib.h>
#include<math.h>
#include<stdio.h>
#include<map>
#include<string>
#include <thread>
#include<algorithm>
using namespace std;
static const int num_threads = 15;
void run(map<int,double> weight,map<int,double> reso,int mass){
    int s=0;
    string var;
    double mynncut;
    string mynnvar;
    double mymasscut;
    int crup;
    char command[200];
    double up,down;
     if(mass>40){
            var="MZ1";
            mynncut=0.9;
            mymasscut=180;
            crup=150;
            mynnvar="DNN_highM";
         
        }
        else{
            var="MZ2";
            mynncut=0;
            mymasscut=110;
            crup=60;
            mynnvar="DNN_lowM";
        }
        up=mass+5.0*reso[mass];
        down=mass-5.0*reso[mass];
        cout<<"fitting "<<mass<<" job now,please wait"<<endl;
        sprintf(command,"source runfit.sh %s %d %f %f %f %f %s %f %d\n",var.c_str(),mass,weight[mass],down,up,mymasscut,mynnvar.c_str(),mynncut,crup);
        system(command);
        cout<<"finish fitting "<<mass<<" please check"<<endl;

}
int main(){
    map<int,double> weight;
    map<int,double> reso;
    int mass1[19]={5,7,9,11,13,15,17,19,23,27,31,35,39,45,51,57,63,69,75};
    ifstream in1;
    ifstream in2;
    int num_job=19;
    int n_threads;
    int k=0;
    int j=0;
    k=num_job;
    thread t[num_threads];
    in1.open("sample_w.txt",ios::out);
    in2.open("sample_re_00.txt",ios::out);
    int mass;
    double reso1,weight1;
    double ratio;
    while(in1>>mass){
        in1>>weight1;
        weight[mass]=weight1;
    }
    while(in2>>mass){
        in2>>reso1;
        reso[mass]=reso1;
    }
    

while(k>0){
    
if(k<=num_threads){
    n_threads=k;
}
else{
    n_threads=num_threads;
}


for (int i = 0; i < n_threads; ++i) {
    t[i] = thread(run,weight,reso,mass1[i+j*num_threads]);
}
for (int i = 0; i < n_threads; ++i) {
    t[i].join();
}
    k=k-n_threads;
    j=j+1;
}
    

    
    
    
    return(0);
    
}
